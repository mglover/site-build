server
{
    include ${deploy.site.path}/conf/security.conf;

	listen 80;
	server_name ${deploy.site.url};

	root ${deploy.site.path}/site/web;

    access_log  ${deploy.site.path}/conf/logs/access.log;
    error_log ${deploy.site.path}/conf/logs/error.log;

    location /
    {
    	try_files $uri /app.php$is_args$args;
	}

    error_page 404 /404.html;
    location = /404.html
    {
        root /usr/share/nginx/html;
    }

    # redirect server error pages to the static page /50x.html
    #
    error_page 500 502 503 504 /50x.html;
    location = /50x.html
    {
        root /usr/share/nginx/html;
    }

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    location ~ ^/(app|app_dev|config)\.php(/|$)
    {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;

	    include fastcgi_params;

        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
	    fastcgi_param REQUEST_URI $uri;
	    fastcgi_param PATH_INFO $uri;

        fastcgi_param ENV ${environment};

	    fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
    }

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    location ~ /\.ht
    {
        deny  all;
    }
}


